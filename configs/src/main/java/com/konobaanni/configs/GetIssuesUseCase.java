package com.konobaanni.configs;

import com.konobaanni.data.entities.IssueWrapper;

/**
 * Created by bozidarkokot on 16/07/2017.
 */


public interface GetIssuesUseCase extends UseCase {


    /**
     * Request datasource the most popular movies
     */
    public void requestIssues();
    public void sendIssuesToPresenter(IssueWrapper issuesWrapper);

    public void unRegister();
}
