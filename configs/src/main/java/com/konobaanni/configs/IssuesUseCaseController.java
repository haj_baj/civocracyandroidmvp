package com.konobaanni.configs;

import com.konobaanni.data.entities.IssueWrapper;

import com.squareup.otto.Bus;

import javax.inject.Inject;

import providers.RestDataSource;

/**
 * Created by bozidarkokot on 16/07/2017.
 */

public class IssuesUseCaseController implements GetIssuesUseCase {

    private final RestDataSource mDataSource;
    private final Bus mUiBus;

    /**
     * Constructor of the class.
     *
     * @param uiBus The bus to communicate the domain module and the app module
     * @param dataSource The data source to retrieve the list of movies
     */
    @Inject
    public IssuesUseCaseController(RestDataSource dataSource, Bus uiBus) {

        mDataSource = dataSource;
        mUiBus = uiBus;
        mUiBus.register(this);
    }

    @Override
    public void requestIssues() {

        mDataSource.getIssues();
    }

    @Override
    public void sendIssuesToPresenter (IssueWrapper response) {

        mUiBus.post(response);
    }

    @Override
    public void unRegister() {

        mUiBus.unregister(this);
    }

    @Override
    public void execute() {

        requestIssues();
    }
}