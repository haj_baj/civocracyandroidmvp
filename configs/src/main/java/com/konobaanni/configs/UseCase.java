package com.konobaanni.configs;

/**
 * Created by bozidarkokot on 16/07/2017.
 */

public interface UseCase {

    void execute ();
}