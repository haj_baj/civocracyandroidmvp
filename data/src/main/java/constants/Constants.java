package constants;

/**
 * Created by bozidarkokot on 16/07/2017.
 */

public class Constants {
    public static String BASE_IMG_URL = "http://res-1.cloudinary.com/civocracy/image/upload/";
    public static String BASE_USER_IMG_URL = "http://res-1.cloudinary.com/civocracy/image/upload/users/";

    public static final String BASE_URL = "https://admin.civocracy.org/api";


}
