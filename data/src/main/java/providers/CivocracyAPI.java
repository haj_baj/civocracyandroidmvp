package providers;

import com.konobaanni.data.entities.IssueWrapper;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by bozidarkokot on 16/07/2017.
 */

public interface CivocracyAPI {






        @GET("/issues?filters[community]=384&filters[official]=1&order_by[date]=desc")
        void getIssues(
            Callback<IssueWrapper> callback);

          }


