package providers;

import com.konobaanni.data.entities.IssueWrapper;
import com.squareup.otto.Bus;

import java.io.Console;
import java.util.List;

import constants.Constants;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bozidarkokot on 16/07/2017.
 */

public class RestSource implements RestDataSource {

    private final CivocracyAPI civocracyAPI;
    private final Bus bus;

    public RestSource(Bus bus) {

        RestAdapter civocracyAPIRest = new RestAdapter.Builder()
                .setEndpoint(Constants.BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.HEADERS_AND_ARGS)
                .build();

        civocracyAPI = civocracyAPIRest.create(CivocracyAPI.class);
        this.bus = bus;
    }


    public Callback retrofitCallback = new Callback() {
        @Override
        public void success(Object o, Response response){
            System.out.print(response.getBody());


            if (o instanceof IssueWrapper) {
                System.out.print(o);
                IssueWrapper issueResponse = (IssueWrapper) o;
                bus.post(issueResponse);

            }





        }

        @Override
        public void failure(RetrofitError error) {

            System.out.printf("[DEBUG]  failure - " + error.getMessage());
        }
    };



    @Override
    public void getUser() {

    }



    @Override
    public void getIssues() {
        civocracyAPI.getIssues(retrofitCallback);
    }
}
