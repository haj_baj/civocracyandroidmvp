package providers;

/**
 * Created by bozidarkokot on 16/07/2017.
 */

public interface RestDataSource {

    void getUser();

    void getIssues();
}