package com.konobaanni.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bozidarkokot on 16/07/2017.
 */

public class UserWrapper implements Serializable {

    @SerializedName("Issue")
    @Expose
    private User user = null;


    public UserWrapper(User user){
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}