package com.konobaanni.data.entities;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.io.Serializable;
import java.util.List;

/**
 * Created by bozidarkokot on 16/07/2017.
 */

public class IssueWrapper implements Serializable {

    @SerializedName("issues")
    @Expose
    private List<Issue> issues = null;


    public IssueWrapper(List<Issue> list){
        this.issues = list;
    }

    public List<Issue> getIssues() {
        return issues;
    }

    public void setIssues(List<Issue> menus) {
        this.issues = menus;
    }

}