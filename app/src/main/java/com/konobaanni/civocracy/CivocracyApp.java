package com.konobaanni.civocracy;

import android.app.Application;

import com.konobaanni.civocracy.Dagger.ApplicationModule;
import com.konobaanni.civocracy.Dagger.DomainModule;
import com.konobaanni.civocracy.dependency_injections.AppComponent;
import com.konobaanni.civocracy.dependency_injections.DaggerAppComponent;

import dagger.internal.DaggerCollections;


/**
 * Created by bozidarkokot on 16/07/2017.
 */

public class CivocracyApp extends Application {

    private AppComponent mAppComponent;

    @Override public void onCreate() {
        super.onCreate();
        this.initializeDependencyInjector();
    }

    private void initializeDependencyInjector() {



        mAppComponent = DaggerAppComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .domainModule(new DomainModule())
                .build();
    }

    public AppComponent getAppComponent() {

        return mAppComponent;
    }
}
