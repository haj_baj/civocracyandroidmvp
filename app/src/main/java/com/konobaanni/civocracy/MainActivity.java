package com.konobaanni.civocracy;

import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.konobaanni.civocracy.Adapters.IssuesListAdapter;
import com.konobaanni.civocracy.Dagger.DaggerIssuesModule;
import com.konobaanni.civocracy.Dagger.IssuesUseCase;
import com.konobaanni.civocracy.Presenters.IssuesPresenter;
import com.konobaanni.civocracy.View.IssuesView;
import com.konobaanni.data.entities.Issue;
import com.konobaanni.data.entities.IssueWrapper;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;
import providers.CivocracyAPI;

public class MainActivity extends AppCompatActivity implements IssuesView {



    private IssuesListAdapter mIssuesAdapter;


    @Optional
    @InjectView(R.id.activity_movies_progress)
    ProgressBar mProgressBar;
    @InjectView(R.id.issues_list)
    ListView mIssuesList;
    @Inject
    IssuesPresenter mIssuesPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        initializeDependencyInjector();

        if (savedInstanceState == null)
            mIssuesPresenter.attachView(this);

        else
            initializeFromParams(savedInstanceState);
    }

    @Override
    protected void onStart() {

        super.onStart();
        mIssuesPresenter.start();
    }

    private void initializeFromParams(Bundle savedInstanceState) {

        IssueWrapper issueWrapper = (IssueWrapper) savedInstanceState
                .getSerializable("issues");

        mIssuesPresenter.onIssuesReceived(issueWrapper);
    }






    private void initializeDependencyInjector() {

        CivocracyApp app = (CivocracyApp) getApplication();


        DaggerIssuesModule.builder()
                .appComponent(app.getAppComponent())
                .issuesUseCase(new IssuesUseCase())
                .build().inject(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

    }


    @Override
    public void showLoading() {

        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {

        mProgressBar.setVisibility(View.GONE);
    }



    @Override
    public boolean isTheListEmpty() {

        return (mIssuesAdapter == null) || mIssuesAdapter.getmIssueList().isEmpty();
    }





    @Override
    protected void onStop() {

        super.onStop();
        mIssuesPresenter.stop();
    }



    @Override
    public void showIssues(List<Issue> issueList) {
        mIssuesAdapter = new IssuesListAdapter(this,0,issueList);
        mIssuesList.setAdapter(mIssuesAdapter);
    }





    @Override
    public void appendIssues(List<Issue> issueList) {
        mIssuesAdapter.appendMenu(issueList);
    }

    @Override
    public Context getContext() {
        return null;
    }


}
