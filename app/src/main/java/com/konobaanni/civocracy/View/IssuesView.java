package com.konobaanni.civocracy.View;

import com.konobaanni.data.entities.Issue;

import java.util.List;

/**
 * Created by bozidarkokot on 16/07/2017.
 */

public interface IssuesView extends MvpView {

    void showIssues(List<Issue> issueList);

    void showLoading ();

    void hideLoading ();

    boolean isTheListEmpty ();

    void appendIssues (List<Issue> issueList);
}