package com.konobaanni.civocracy.View;

/**
 * Created by bozidarkokot on 16/07/2017.
 */

public interface MvpView {

    public android.content.Context getContext();
}