package com.konobaanni.civocracy.Presenters;

/**
 * Created by bozidarkokot on 16/07/2017.
 */

import com.konobaanni.civocracy.View.IssuesView;
import com.konobaanni.configs.GetIssuesUseCase;
import com.konobaanni.data.entities.IssueWrapper;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;


public class IssuesPresenter extends Presenter {

private final Bus mBus;
private GetIssuesUseCase mGetIssues;
private IssuesView mIssuesView;

private boolean isLoading = false;
private boolean mRegistered;

@Inject
public IssuesPresenter( GetIssuesUseCase getMoviesUsecase, Bus bus) {

        mGetIssues    = getMoviesUsecase;
        mBus = bus;
        }

public void attachView (IssuesView issuesView) {

        mIssuesView = issuesView;
        }

@Subscribe
public void onIssuesReceived(IssueWrapper issueWrapper) {

        if (mIssuesView.isTheListEmpty()) {

        mIssuesView.hideLoading();

               mIssuesView.showIssues(issueWrapper.getIssues());

        } else {
                mIssuesView.appendIssues(issueWrapper.getIssues());

        }

        isLoading = false;
        }




@Override
public void start() {

        if (mIssuesView.isTheListEmpty()) {

        mBus.register(this);
        mRegistered = true;

        mIssuesView.showLoading();
        mGetIssues.execute();
        }
        }

@Override
public void stop() {
        }


        }