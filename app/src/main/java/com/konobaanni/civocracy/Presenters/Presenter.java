package com.konobaanni.civocracy.Presenters;

/**
 * Created by bozidarkokot on 16/07/2017.
 */

public abstract class Presenter {

    /**
     * Called when the presenter is initialized
     */
    public abstract void start ();

    /**
     * Called when the presenter is stop, i.e when an activity
     * or a fragment finishes
     */
    public abstract void stop ();
}