package com.konobaanni.civocracy.Dagger;

import android.content.Context;

import com.konobaanni.civocracy.CivocracyApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import providers.CivocracyAPI;

/**
 * Created by bozidarkokot on 16/07/2017.
 */


@Module
public class ApplicationModule {

    private final CivocracyApp application;

    public ApplicationModule(CivocracyApp application) {

        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return application;
    }



}