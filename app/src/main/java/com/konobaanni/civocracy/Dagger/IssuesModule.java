package com.konobaanni.civocracy.Dagger;

import com.konobaanni.civocracy.MainActivity;
import com.konobaanni.civocracy.dependency_injections.AppComponent;

import dagger.Component;

/**
 * Created by bozidarkokot on 16/07/2017.
 * Interface to inject Issue instance to activity
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules = IssuesUseCase.class)
public interface IssuesModule {

    void inject (MainActivity mainActivity);
}
