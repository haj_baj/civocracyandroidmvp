package com.konobaanni.civocracy.Dagger;

import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import providers.RestSource;

/**
 * Created by bozidarkokot on 16/07/2017.
 */

@Module
public class DomainModule {

    @Provides
    @Singleton
    Bus provideBus () {
        return new Bus();
    }

    @Provides @Singleton
    RestSource provideDataSource (Bus bus) { return new RestSource(bus); }

}
