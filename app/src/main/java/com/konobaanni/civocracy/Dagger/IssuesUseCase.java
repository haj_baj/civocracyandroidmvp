package com.konobaanni.civocracy.Dagger;

import com.konobaanni.configs.GetIssuesUseCase;
import com.konobaanni.configs.IssuesUseCaseController;
import com.squareup.otto.Bus;

import dagger.Module;
import dagger.Provides;
import providers.RestSource;

/**
 * Created by bozidarkokot on 16/07/2017.
 * Class to provide issues to Activity
 */

@Module
public class IssuesUseCase {



    @Provides
    GetIssuesUseCase provideIssuesUseCase (Bus bus, RestSource movieSource) {
        return new IssuesUseCaseController(movieSource, bus);
    }
}