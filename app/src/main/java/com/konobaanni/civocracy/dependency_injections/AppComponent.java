package com.konobaanni.civocracy.dependency_injections;

import com.konobaanni.civocracy.Dagger.ApplicationModule;
import com.konobaanni.civocracy.Dagger.DomainModule;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Component;
import providers.RestSource;

/**
 * Created by bozidarkokot on 16/07/2017.
 */

@Singleton
@Component(modules = {
        ApplicationModule.class,
        DomainModule.class,
})

public interface AppComponent {

    Bus bus();
    RestSource restSource();
}