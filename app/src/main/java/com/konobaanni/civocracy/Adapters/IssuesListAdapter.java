package com.konobaanni.civocracy.Adapters;

import android.content.Context;
import android.provider.SyncStateContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.konobaanni.civocracy.R;
import com.konobaanni.data.entities.Issue;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by bozidarkokot on 16/07/2017.
 */

public class IssuesListAdapter extends ArrayAdapter<Issue> {

    private int layoutResource;
    private List<Issue> mIssueList;

    public IssuesListAdapter(Context context, int layoutResource, List<Issue> threeStringsList) {
        super(context, layoutResource, threeStringsList);
        this.layoutResource = layoutResource;
        this.mIssueList = threeStringsList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(R.layout.issues_list_item, null);
        }

        Issue issue = getItem(position);

        if (issue != null) {

            ImageView discusionImage = (ImageView) view.findViewById(R.id.discusion_image);
            ImageView issuesUserImg = (ImageView) view.findViewById(R.id.issues_user);


            TextView disusionName = (TextView) view.findViewById(R.id.discusion_name);
            TextView lastActive = (TextView) view.findViewById(R.id.last_active);
            TextView discusionDescription = (TextView) view.findViewById(R.id.discusion_desc);
            TextView followers = (TextView) view.findViewById(R.id.followers);


            Picasso.with(getContext())
                    .load(issue.getUserImgUrl())
                    .placeholder(android.R.drawable.ic_dialog_alert)
                    .error(android.R.drawable.ic_dialog_alert)
                    .into(issuesUserImg);

            Picasso.with(getContext())
                    .load(issue.getImage())
                    .placeholder(android.R.drawable.ic_dialog_alert)
                    .error(android.R.drawable.ic_dialog_alert)
                    .into(discusionImage);

            disusionName.setText(issue.getTag());
            discusionDescription.setText(issue.getSummary());
            followers.setText(issue.getFollowers());
            lastActive.setText(issue.getLastActiveDate());



        }

        return view;
    }

    public List<Issue> getmIssueList() {

        return mIssueList ;
    }

    public void appendMenu(List<Issue> issueList) {
        List<Issue> mMenu2 = issueList;
        issueList.clear();
        issueList.addAll(mMenu2);
        notifyDataSetChanged();
    }

}